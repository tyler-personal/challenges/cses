import Data.Array.IO
import Data.IORef

data Person = Nil (IORef Int) | Slave Person (IORef Int)

count :: Person -> IORef Int
count (Nil c) = c
count (Slave _ c) = c

main :: IO ()
main = do
  n <- readLn
  nums <- map read . words <$> getLine

  slaves <- newIORef 0 >>= newArray (0,n) . Nil
  mapM_ (addSlave slaves) (zip [2..] nums)
  counts <- getElems slaves >>= mapM (readIORef . count)
  putStrLn . unwords . map show $ drop 1 counts

addSlave :: IOArray Int Person -> (Int, Int) -> IO ()
addSlave slaves (i,n) = do
  owner <- readArray slaves n
  newIORef 0 >>= writeArray slaves i . Slave owner
  incrementOwners owner

incrementOwners :: Person -> IO ()
incrementOwners (Nil c) = do
  modifyIORef c (+ 1)
  return ()
incrementOwners (Slave p c) = do
  modifyIORef c (+ 1)
  incrementOwners p

