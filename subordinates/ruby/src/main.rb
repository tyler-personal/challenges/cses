n = gets.to_i
# p (1..n).map { |x| [x,[]] }.to_h.merge

gets.split.map
    .with_index { |x, i| [x.to_i, i+2] }
    .inject({}) { |xs, (owner, slave)| xs.merge({owner => [slave]}) { |k, a, b| a + b } }
    .sort.to_h
