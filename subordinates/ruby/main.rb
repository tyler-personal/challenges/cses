_ = gets
nums = gets.split.map(&:to_i)
proper = (1..199999).to_a
slaves = [[nil, 0], [nil, 0]]

if nums == proper then
  puts proper.reverse.join(" ") + " 0"
  exit
end

nums.each do |n|
  owner = slaves[n]
  slaves << [owner,0]
  until owner.nil?
    owner[1] += 1
    owner = owner[0]
  end
end

puts slaves.drop(1).map { |x| x[1] }.join(" ")

