n=gets.to_i
a=(1..n).map{|x|[x,[]]}.to_h.merge(gets.split.map
  .with_index{|x,i|[x.to_i,i+2]}
  .inject({}){|x,(o, s)|x.merge({o=>[s]}){|k,a,b|a+b}}
  .to_h)

f= ->(o){a[o].length+a[o].map{|x|f.call x}.sum}
p (1..n).map{|x|f.call x}
