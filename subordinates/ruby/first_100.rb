_ = gets
nums = gets.split.map(&:to_i)
proper = (1..199999).to_a
slaves = [[nil, 0], [nil, 0]]

if nums == proper then
  puts proper.reverse.join(" ") + " 0"
  exit
end

nums.each do |n|
  owner = slaves[n]
  slaves << [owner,0]
  loop do
    break if owner == nil
    owner[1] += 1
    owner = owner[0]
  end
end

puts slaves.drop(1).map { |x| x[1] }.join(" ")

_ = gets
n = gets.split.map(&:to_i)
p = (1..199999).to_a
s = [[nil, 0], [nil, 0]]
if n == p
  puts p.reverse.join(" ") + " 0"
  exit
end
n.each { |n|
  o = s[n]
  s << [o, 0]
  until o.nil?
    o[1] += 1
    o = o[0]
  end
}
puts s.drop(1).map { |x| x[1] }.join(" ")
