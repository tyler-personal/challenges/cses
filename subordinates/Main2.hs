{-# LANGUAGE TypeApplications, NoMonomorphismRestriction, ExtendedDefaultRules #-}
module Main2 where
import Control.Category hiding ((.))

data Slave = Slave { owner :: Maybe Slave, count :: Int }

mkSlave o = Slave { owner = o, count = 0 }

main = do
  _ <- getLine
  nums <- map (read @Int) . words <$> getLine
  putStrLn . unwords . map (show . count) . drop 1
    $ foldl addSlave [mkSlave Nothing, mkSlave Nothing] nums

addSlave :: [Slave] -> Int -> [Slave]
addSlave slaves num = undefined
